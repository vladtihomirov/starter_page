import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {GoogleMeetComponent} from "./google-meet/google-meet.component";
import {JiraComponent} from "./jira/jira.component";
import {JenkinsComponent} from "./jenkins/jenkins.component";
import {RancherComponent} from "./rancher/rancher.component";
import {GitlabComponent} from "./gitlab/gitlab.component";

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/home'},
  {component: HomeComponent, path: 'home'},
  {component: GitlabComponent, path: 'gitlab'},
  {component: RancherComponent, path: 'rancher'},
  {component: JenkinsComponent, path: 'jenkins'},
  {component: JiraComponent, path: 'jira'},
  {component: GoogleMeetComponent, path: 'google-meet'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
