import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from './home/home.component';
import { GitlabComponent } from './gitlab/gitlab.component';
import { RancherComponent } from './rancher/rancher.component';
import { JenkinsComponent } from './jenkins/jenkins.component';
import { JiraComponent } from './jira/jira.component';
import { GoogleMeetComponent } from './google-meet/google-meet.component';
import { ProjectWidgetComponent } from './project-widget/project-widget.component';
import { ServiceWidgetComponent } from './service-widget/service-widget.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GitlabComponent,
    RancherComponent,
    JenkinsComponent,
    JiraComponent,
    GoogleMeetComponent,
    ProjectWidgetComponent,
    ServiceWidgetComponent,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
